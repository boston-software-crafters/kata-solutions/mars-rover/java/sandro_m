# Kata Starter Project for Java using IntelliJ and Maven

## Prerequisites

To be successful with this kata, you, the developer(s), should have IntelliJ CE (Community Edition) or better installed on your machine.

Java SE only needs to be installed if you plan on running your code outside of IntelliJ.

You should be familiar with Git and a git client: command line, IntelliJ, GitHub Desktop, or some other GUI tool.  The Emacs Magit library is highly recommended, but only if you already know Emacs.

Basic understanding of GitHub or GitLab flow and an account on one or both of these platforms is also required.

If you feel weak in any of these areas, reach out to one of the facilitators or the organizer for some bootstrapping help.

## Kata Instructions

Once you, the developer(s) are ready to start, do the following:

1) Verify the environment is ready by clicking on the green arrow in the file `src/test/java/org/bsc/katas/SolutionTest.java` on line 6.

This test should pass verifying that the environment is ready for a solution.

2) Add your first real test using the pattern from the test named `verify the development environment works`

3) Each time you write enough test code to eliminate all compilation errors, re-run all the tests in the `SolutionTest` class.

For any and all tests that fail, fix the production solution in the file `src/main/java/org/bsc/katas/Solution.java`.

Complete instructions for the kata can be found on the [Katalyst Site](https://katalyst.codurance.com/mars-rover) which includes a video of Sandro Mancuso solving the Mars Rover Kata.

You are encouraged to watch the video, even before attempting it yourself, but this is optional. If you do so, try to take a different approach when doing your solution.

In fact, if you have the time, check out any number of solutions ahead of time. The solution is not that important. The TDD process, once you have a solution in mind, is important, as is collaborating with your team.

So if you do look at other solutions, please avoid looking at the associated tests. That defeats the whole point of the exercise. Comparing after the fact might be a good thing to do.

The first step in solving the kata is to understand the constraints. Once you think you understand them, list your assumptions. Sandro does a nice job at this near the beginning of his video.

With your basic understanding, then start devising tests that yield the production code that solves the problem. The examples on the Katalyst Site are a good choice for tests.

As you build a suite of tests, you might see patterns emerge that are good to use in your testing. Go for it.

But most of all, have some fun.

## Challenges [optional]

If you are already in the Internet Hall Of Fame and have finished this kata before everyone else has read the README.md file, then here are some challenges for you:

1) Do a functional programming style solution if you've already done an OO solution. Or vice-versa. Which solution is better? faster?

2) Make your solution clean, as in the principles and practices of Robert "Uncle Bob" Martin and his Clean Code world: use good names, short functions, good structure, SOLID code, etc.

## Solution Notes

...
